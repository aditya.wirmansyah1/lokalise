<?php

namespace App\Repository;

use App\Entity\Translation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Translation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Translation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Translation[]    findAll()
 * @method Translation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Translation::class);
    }

    /**
     * remove all from table
     */
    public function removeAll(string $iso_code)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'DELETE FROM translation WHERE language_iso = ?';

        $statement = $conn->prepare($sql);

        return $statement->execute([0 => $iso_code]);
    }

    /**
     * @return Translation[]
     */
    // public function findAllByLanguageIso(string $language_iso): array
    // {
    //     $conn = $this->getEntityManager()->getConnection();

    //     $sql = '
    //         SELECT t.value AS translation, k.name AS `key`
    //         FROM translation AS t 
    //         LEFT JOIN `key` k 
    //             ON k.id = t.key_id 
    //         LEFT JOIN languages l 
    //             ON l.id = t.language_id
    //         WHERE l.iso_code = ? ORDER BY t.value ASC
    //         ';

    //     return $conn->fetchAllAssociative($sql, [ 0 => $language_iso]);
    // }

    /**
     * @return Translation
     */
    // public function findOneByKey(string $key, string $language_iso): array
    // {
    //     $conn = $this->getEntityManager()->getConnection();

    //     $sql = '
    //         SELECT t.value AS translation, k.name AS `key`
    //         FROM translation AS t 
    //         LEFT JOIN `key` k 
    //             ON k.id = t.key_id 
    //         LEFT JOIN languages l 
    //             ON l.id = t.language_id
    //         WHERE k.name = ? AND l.iso_code = ?
    //         ';

    //     return $conn->fetchAssociative($sql, [0 => $key, 1 => $language_iso]);
    // }

    /**
     * @return Boolean
     */
    // public function updateTranslate(string $key, string $language_iso, 
    //     string $translation)
    // {
    //     $conn = $this->getEntityManager()->getConnection();

    //     $sql = '
    //         UPDATE translation t 
    //         LEFT JOIN `key` k 
    //             ON k.id = t.key_id 
    //         LEFT JOIN languages l 
    //             ON l.id = t.language_id
    //         SET t.value = ?
    //         WHERE k.name = ? AND l.iso_code = ?
    //         ';

    //     $statement = $conn->prepare($sql);

    //     return $statement->execute([
    //         0 => $translation, 
    //         1 => $key, 
    //         2 => $language_iso]);
    // }

    // /**
    //  * @return Translation[] Returns an array of Translation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Translation
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
