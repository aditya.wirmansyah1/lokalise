<?php

namespace App\Controller;

use App\Entity\Languages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class LanguagesController extends AbstractController
{
    /**
     * @Route("/languages", methods={"GET"})
     */
    public function languages(): JsonResponse
    {
        $languages = $this->getDoctrine()
            ->getRepository(Languages::class)
            ->findAll();

        $data = [];
        foreach ($languages as $lang) {
            $data[] = [
                'id' => $lang->getId(),
                'name' => $lang->getName(),
                'isoCode' => $lang->getIsoCode()
            ];
        }
    
        return new JsonResponse($data, Response::HTTP_OK);
    }
}
