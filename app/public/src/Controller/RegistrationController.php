<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/registration", name="registration", methods={"POST"})
     */
    public function registration(Request $request, 
        UserPasswordHasherInterface $passwordHasher): Response
    {
        $data = json_decode($request->getContent(), true);
        $email = $data['email'];
        $password = $data['password'];
        $roles = $data['roles'];

        if (empty($email) || empty($password)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }       

        $entityManager = $this->getDoctrine()->getManager();
        $user = new User();
        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $password
        );        
        $newRoles = [$roles];
        $user->setPassword($hashedPassword);
        $user->setEmail($email);
        $user->setRoles($newRoles);

        $entityManager->persist($user);

        $entityManager->flush();

        return $this->json([
            'message'  => "register success",
        ], Response::HTTP_OK);
    }
}
