<?php

namespace App\Controller;

use App\Entity\Key;
use App\Entity\Translation;
use App\Entity\Languages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Yaml;
use App\Form\FileUploadType;
use App\Service\FileUploader;
use ZipArchive;

class KeyController extends AbstractController
{
    /**
     * @Route("/key-upload", name="key.upload", methods={"POST"})
     */
    public function upload(Request $request, FileUploader $file_uploader): Response
    {
        $file = $request->files->get('upload_file');
        if ($file) 
        {
            $file_name = $file_uploader->upload($file);
            if (null !== $file_name) {
                $directory = $file_uploader->getTargetDirectory();
                $full_path = $directory.'/'.$file_name;
                
                $zip = new ZipArchive();
                $res = $zip->open($full_path);
                if ($res === TRUE) {
                    $arrKey = $arrLang = $arrTrans = [];
                    $zip->extractTo($directory);
                    $zip->close();

                    chdir($directory);
                    foreach (glob("*.json") as $file) {                        
                        $json = file_get_contents($file);
                        $data = json_decode($json, TRUE);
                        $jsonFilename = basename($file, ".json");
                        $arrFilename = explode("-", $jsonFilename);
                        if(count($arrFilename) < 2) {
                            throw new NotFoundHttpException('invalid json filename!');
                        }
                        $arrLang[strval($arrFilename[1])] = $arrFilename[0];
                        $arrTrans[strval($arrFilename[1])] = $data;        
                        foreach($data as $k=>$val) {
                            $arrKey[] = $k;                                             
                        }
                        unlink($file);
                    }               

                    foreach (glob("*.yaml") as $file) {  
                        $arrData = Yaml::parseFile($file);

                        foreach($arrData as $key=> $Arr) {
                            $arrTmp = explode(".", $key);
                            $arrLang[strval($arrTmp[1])] = $arrTmp[0];
                            $arrTrans[strval($arrTmp[1])] = $Arr;    

                            foreach($Arr as $k=>$val) {
                                $arrKey[] = $k;                                             
                            }
                        }                        
                    }
                    
                    // save data
                    $this->saveKey($arrKey);
                    $this->saveLang($arrLang);
                    $this->saveTrans($arrTrans);
                } 
                unlink($full_path);
            }
            else {
                return $this->json([
                    'message' => "Failed to upload file"], 
                    Response::HTTP_OK
                );
            }
        }

        return $this->json([
            'message' => "File successfuly uploaded, translation data is updated!"], 
            Response::HTTP_OK
        );
    }
    

    /**
     * @Route("/key-create", name="key.create", methods={"POST"})
     */
    public function keyCreate(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $name = $data['name'];

        if (empty($name)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }       

        $entityManager = $this->getDoctrine()->getManager();
        $key = new Key();     
        $key->setName($name);
        $entityManager->persist($key);
        $entityManager->flush();

        return $this->json([
            'message'  => "key created!",
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/key-rename", name="key.rename", methods={"POST"})
     */
    public function keyRename(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $id = $data['id'];
        $name = $data['name'];

        if (empty($name) || empty($id)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }  

        $entityManager = $this->getDoctrine()->getManager();
        $key = $entityManager->getRepository(Key::class)->find($id);
        
        if (!$key) {
            throw $this->createNotFoundException(
                'No key found for id '.$id
            );
        }

        $key->setName($name);
        $entityManager->persist($key);
        $entityManager->flush();

        return $this->json([
            'message'  => "key updated!",
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/key-delete", name="key.delete", methods={"DELETE"})
     */
    public function keyDelete(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $id = $data['id'];

        if (empty($id)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }  

        $entityManager = $this->getDoctrine()->getManager();
        $key = $entityManager->getRepository(Key::class)->find($id);
        
        if (!$key) {
            throw $this->createNotFoundException(
                'No key found for id '.$id
            );
        }

        $entityManager->remove($key);
        $entityManager->flush();

        return $this->json([
            'message'  => "key deleted!",
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/key-list", name="key.list", methods={"GET"})
     */
    public function keyList(Request $request): Response
    {
        $keys = $this->getDoctrine()
            ->getRepository(Key::class)
            ->findAll();

        $data = [];
        foreach ($keys as $key) {
            $data[] = [
                'id' => $key->getId(),
                'name' => $key->getName()
            ];
        }
    
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/key-retrieve/{id}", name="key.retrieve", methods={"GET"})
     */
    public function keyRetrieve(int $id): Response
    {
        $key = $this->getDoctrine()
            ->getRepository(Key::class)
            ->find($id);

        if (!$key) {
            throw $this->createNotFoundException(
                'No key found for id '.$id
            );
        }

        $data = [
            'id' => $key->getId(),
            'name' => $key->getName()
        ];
    
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * function helper array
     */
    function compareDeepValue($val1, $val2)
    {
        if(!empty($val1))
            return strcmp($val1['name'], $val2['name']);
        else return 1;
    }

    /**
     * save imported keys from zip file
     */
    private function saveKey($arrKey)
    {
        $repository = $this->getDoctrine()->getRepository(Key::class);
        $existingKeys = $repository->findAllList();
        if(count($existingKeys) > 0)
            $existingKeys = array_column($existingKeys, "name");

        $newKeys = array_unique (array_merge ($existingKeys, $arrKey));

        // remove data                
        $repository->removeAll();

        // insert data
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($newKeys as $val) {
            $key = new Key();     
            $key->setName($val);
            $entityManager->persist($key);
        }
        $entityManager->flush();        
    }

    /**
     * save imported language from zip file
     */
    private function saveLang($arrLang)
    {
        $repository = $this->getDoctrine()->getRepository(Languages::class);
        $existingLangs = $repository->findAllList();
        if(count($existingLangs) > 0) {
            $existingLangs = array_combine(array_column($existingLangs, 'iso_code'), 
                        array_column($existingLangs, 'name') );
        }
        
        $newLangs = $arrLang + $existingLangs;        
       
        // remove data                
        $repository->removeAll();

        // insert data
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($newLangs as $key=>$val) {
            $lang = new Languages();     
            $lang->setName($val);
            $lang->setIsoCode($key);
            $entityManager->persist($lang);
        }
        $entityManager->flush(); 
    }

    /**
     * save imported translation from zip file
     */
    private function saveTrans($arrTrans)
    {
        $repository = $this->getDoctrine()->getRepository(Translation::class);
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($arrTrans as $iso_code => $Arr) {
            $repository->removeAll($iso_code);

            foreach ($Arr as $key=>$val) {
                $trans = new Translation();     
                $trans->setValue($val);
                $trans->setTransKey($key);
                $trans->setLanguageIso($iso_code);
                $entityManager->persist($trans);
            }
            $entityManager->flush(); 
        }
    }


}
