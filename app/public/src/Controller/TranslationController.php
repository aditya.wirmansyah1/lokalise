<?php

namespace App\Controller;

use App\Entity\Key;
use App\Entity\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class TranslationController extends AbstractController
{
    /**
     * @Route("/translation-all", name="translation.all", methods={"GET"})
     */
    public function translateAll(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $language_iso = $data['language_iso'];

        if (empty($language_iso)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }  

        $translation = $this->getDoctrine()
            ->getRepository(Translation::class)
                ->findBy(['language_iso' => $language_iso]);
        $data = [];
        foreach ($translation as $trans) {
            $data[] = [
                'key' => $trans->getTransKey(),
                'translation' => $trans->getValue()
            ];
        }
    
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/translation", name="translation", methods={"GET"})
     */
    public function translate(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $key = $data['key'];
        $language_iso = $data['language_iso'];

        if (empty($key) || empty($language_iso)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }  

        $translation = $this->getDoctrine()
            ->getRepository(Translation::class)
            ->findOneBy([
                'trans_key' => $key,
                'language_iso' => $language_iso,
            ]);

        $data = [
            'key' => $translation->getTransKey(),
            'translation' => $translation->getValue()
        ];
    
        return new JsonResponse($data, Response::HTTP_OK);
    }

     /**
     * @Route("/translation-update", name="translation.update", methods={"POST"})
     */
    public function translateUpdate(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $key = $data['key'];
        $language_iso = $data['language_iso'];
        $translation = $data['translation'];

        if (empty($key) || empty($language_iso) || empty($translation)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }  

        $entityManager = $this->getDoctrine()->getManager();
        $trans = $entityManager->getRepository(Translation::class)->findOneBy([
            'trans_key' => $key,
            'language_iso' => $language_iso,
        ]);
        
        if (!$trans) {
            throw $this->createNotFoundException(
                'No translation found for id '.$id
            );
        }

        $trans->setValue($translation);
        $entityManager->persist($trans);
        $entityManager->flush();
    
        return $this->json([
            'message'  => "Success update translation!",
        ], Response::HTTP_OK);
    }
}
